package business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import app.domain.Question;

public class RandomGenerator {
	
	private static RandomGenerator rg;
	private int randomInt;
	private int counter;
	private ArrayList<Integer> rNums;
	
	private RandomGenerator(){
		rNums = new ArrayList<Integer>();
	}
	
	public static RandomGenerator getInstance(){
		return rg = new RandomGenerator();
	}
	
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public int getRandomInt() {
		return randomInt;
	}

	public void setRandomInt(int randomInt) {
		this.randomInt = randomInt;
	}

	
	public int getRand(){
		
		 Random randomGenerator = new Random();
		   int randomInt = randomGenerator.nextInt(3);
		    
		   //System.out.println(randomNum);
		   return randomInt;
		}
		
		public  ArrayList<Integer> getRandoms(){
			int n = 0;
			 Random randomGenerator = new Random();
			    for (int idx = 0; idx < 3; ++idx){  
			    	rNums.add(idx);     
			    }
			    Collections.shuffle(rNums);
			    System.out.println(rNums);
				return rNums;
			}
		
		public synchronized int getCount(){
			counter++;
			return counter;
		}
}

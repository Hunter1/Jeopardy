package app.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;





import business.RandomGenerator;
import app.domain.Question;
import app.domain.QuestionBank;

public class Player2 extends HttpServlet{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String ans1 = req.getParameter("ans1");
		System.out.println(ans1);
		String ans2 = req.getParameter("ans2");
		System.out.println(ans2);
		
		if( ans1!= null && ans1=="true"){
		    
			resp.sendRedirect("1fail.jsp");
			/*RequestDispatcher rd = req.getRequestDispatcher("1Success.jsp");
		    rd.forward(req, resp);*/
			
		}else if (ans1!=null && ans2=="false"){
			
			RequestDispatcher rd = req.getRequestDispatcher("1Success.jsp");
			rd.forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		HttpSession session = req.getSession();
		
		QuestionBank QB = new QuestionBank();
		ArrayList<Question> list =QB.getBank();
		
	
	    RandomGenerator gq = RandomGenerator.getInstance();
	    int n = gq.getRand();
	    System.out.println(n);
	      
	    Question q2 = list.get(n);
	    System.out.println(q2);
	    session.setAttribute("player2", q2);
		//session.getAttribute("player1");
		
        RequestDispatcher rd = req.getRequestDispatcher("Answer2.jsp");
		
		rd.forward(req, resp);
	}
}

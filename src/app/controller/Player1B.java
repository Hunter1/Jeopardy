package app.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Player1B extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String ans1 = req.getParameter("ans1");
		System.out.println(ans1);
		String ans2 = req.getParameter("ans2");
		System.out.println(ans2);
		
		if( ans1!= null && ans1=="true"){
		    
			//resp.sendRedirect("1fail.jsp");
			RequestDispatcher rd = req.getRequestDispatcher("1Success.jsp");
		    rd.forward(req, resp);
			
		}else if (ans1!=null && ans2=="false"){
			
			resp.sendRedirect("1Success.jsp");
			/*RequestDispatcher rd = req.getRequestDispatcher("1Success.jsp");
			rd.forward(req, resp);*/
		}
	}
}

package app.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;





import business.RandomGenerator;
import app.domain.Question;
import app.domain.QuestionBank;

public class Player1 extends HttpServlet{
  
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String ans1 = req.getParameter("ans1");
		System.out.println("The answer is: "+ ans1);
		String ans2 = req.getParameter("ans2");
		System.out.println(ans2);
		
		if( ans1!= null && ans1.equals("true")){
		    System.out.println("In the if statement" + ans1);
			resp.sendRedirect("1fail.jsp");
			/*RequestDispatcher rd = req.getRequestDispatcher("1Success.jsp");
		    rd.forward(req, resp);*/
			
		}else if (ans1!=null && ans2.equals("false")){
			
			RequestDispatcher rd = req.getRequestDispatcher("1Success.jsp");
			rd.forward(req, resp);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		HttpSession session = req.getSession();
		
		QuestionBank QB = new QuestionBank();
		ArrayList<Question> list =QB.getBank();
		
	
	    RandomGenerator gq = RandomGenerator.getInstance();
	    int n = gq.getRand();
	    System.out.println(n);
	      
	    Question q = list.get(n);
	    System.out.println(q);
	    
		session.setAttribute("player1",q );
		
        RequestDispatcher rd = req.getRequestDispatcher("Answer1.jsp");
		
		rd.forward(req, resp);
	}
}

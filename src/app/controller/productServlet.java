package app.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import app.domain.Product;

public class productServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ArrayList<Product> products = new ArrayList<Product>();
		Product p1 = new Product();
		Product p2 = new Product();
		Product p3 = new Product();
		
		p1.setName("Mustard");
		p1.setDesc("h");
		p1.setPrice(2.34);
		
		p2.setName("burger");
		p2.setDesc("y");
		p2.setPrice(2.76);
		
		p3.setName("ketchup");
		p3.setDesc("p");
		p3.setPrice(1.00);
		
		products.add(p1);
		products.add(p2);
		products.add(p3);
		
		HttpSession session = req.getSession();
		
		session.setAttribute("productList", products);
		
		session.setAttribute("p1", p1);
		
		RequestDispatcher rd = req.getRequestDispatcher("product.jsp");
		
		rd.forward(req, resp);
			
	}

}

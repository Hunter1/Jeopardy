package app.domain;

public class Question {
	private String question;
	private String ans;
	private int dollar;
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	
	@Override
	public String toString() {
		return "Question [question=" + question + ", ans=" + ans + "]";
	}
	public int getDollar() {
		return dollar;
	}
	public void setDollar(int dollar) {
		this.dollar = dollar;
	}
	
}

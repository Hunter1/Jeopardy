package app.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import business.RandomGenerator;

public class QuestionBank {
	
	private ArrayList<Question> bank;
	
	public ArrayList<Question> getBank(){
		return this.bank;
	}
	
	public QuestionBank(){
		bank = new ArrayList<Question>();
		Question q1 = new Question();
		q1.setQuestion("Is ActionServlet Spring controller?");
		q1.setAns("false");
		q1.setDollar(650);
		
		Question q2 = new Question();
		q2.setQuestion("StringBuffer is sychronized?");
		q2.setAns("true");
		q2.setDollar(300);
		
		Question q3 = new Question();
		q3.setQuestion("JSP means Java Servlet Page?");
		q3.setAns("false");
		q3.setDollar(500);
		
		bank.add(q1);
		bank.add(q2);
		bank.add(q3);
	}

	@Override
	public String toString() {
		return "QuestionBank [bank=" + bank + "]";
	}

	

}
